import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listaTareas: null,
    accionActual: {modo: 'alta', tarea: {}},
    loading:{titulo: '', estado: false, color: 'primary' },
    URLAPI: 'http://www.area54sa.com.ar:8123/api/tareas/'  
  },
  mutations: {
    mostrarLoading(state, payload) {
      state.loading.titulo=payload.titulo
      state.loading.estado=true
      state.loading.color=payload.color
    } ,
    ocultarLoading(state) {
      state.loading.titulo=''
      state.loading.estado=false
    },
    async agregarTarea(state, nuevaTarea){
      this.commit('mostrarLoading', {titulo: "Grabando nueva tarea"})
      let request=state.URLAPI+'crearoactualizar'
      request+="?titulo="+nuevaTarea.titulo
      request+="&detalle="+nuevaTarea.detalle
      await axios.get(request)
      this.commit('leerTareasDeAPI')
      this.commit('ocultarLoading')
    },
    async eliminarTarea(state, IdAEliminar) {
      this.commit('mostrarLoading', {titulo: "Eliminando tarea"})
      let request=state.URLAPI+'eliminar'
      request+="?id="+IdAEliminar
      await axios.get(request)
      this.commit('leerTareasDeAPI')
      this.commit('ocultarLoading')
    },
    cambiarAccionActual(state, payload) {
      state.accionActual=payload
    },
    async actualizarTarea(state, payload) {
      const posicionElemento=state.listaTareas.findIndex(tarea => tarea.id==payload.id)
      if (posicionElemento>=0){
        this.commit('mostrarLoading', {titulo: "Registrando información"})
        let request=state.URLAPI+'crearoactualizar'
        request+="?titulo="+payload.titulo
        request+="&detalle="+payload.detalle
        request+="&id="+payload.id
        await axios.get(request)
        this.commit('leerTareasDeAPI')
        this.commit('ocultarLoading')
      }
    },
    async leerTareasDeAPI(state) {
      this.commit('mostrarLoading', {titulo: "Leyendo información"})
      const TareasLeidas= []
      const datos=await axios.get(state.URLAPI+'gettodas')
      datos.data.forEach(element => {
        TareasLeidas.push({id: element.Id, titulo: element.Titulo, detalle: element.Detalle})
      })
      state.listaTareas=TareasLeidas;
      this.commit('ocultarLoading')
    }
  },
  //,
  //getters: {
  //  accionActual: state => {return state.accionActual}
  //},
  actions: {
    mostrarLoading(state, payload) {
      this.commit('mostrarLoading', payload)
    },
    ocultarLoading(state) {
      this.commit('ocultarLoading')
    },
    agregarTarea(state, nuevaTarea){
      this.commit('agregarTarea', nuevaTarea)
    },
    actualizarTarea(state, payload) {
      this.commit('actualizarTarea', payload)
    },
    actualizarAccionActual(state, nuevaAccion) {
      this.commit('cambiarAccionActual', nuevaAccion)
    },
    cargarListaTareas(state){
      this.commit('leerTareasDeAPI')
    }

  },
  modules: {
  }
})
